# Telnet

Реализовать telnet клиент (см man telnet)
- клиент должен поддерживать опцию timeout, ограничивающую время подключения к серверу.
- клиент должен обрабатывать нажатие `Ctrl+C` для быстрого выхода.

## Реализация

Используется два таймаута - на установление соединения и на общее время сессии.

```
go run main.go [-timeout] [-dial-timeout] host [port]
```

Таймауты в командной строке необходимо указывать с единицей измерения, например

```
go run main.go -timeout 60s -dial-timeout 5s yandex.ru 80
```

## Пример использования
(Оказывается очень трудно найти сайт без принудительного редиректа на https)

```
go run main.go example.com
Trying: 93.184.216.34:80
Connected to: example.com:80
Use ^C to exit
GET / HTTP/1.1
Host: example.com

HTTP/1.1 200 OK
Cache-Control: max-age=604800
Content-Type: text/html; charset=UTF-8
Date: Sat, 17 Aug 2019 12:17:12 GMT
Etag: "1541025663+ident"
Expires: Sat, 24 Aug 2019 12:17:12 GMT
Last-Modified: Fri, 09 Aug 2013 23:54:35 GMT
Server: ECS (nyb/1D2C)
Vary: Accept-Encoding
X-Cache: HIT
Content-Length: 1270

...

^C
Got signal: interrupt
Good bye!
Connection closed
```


