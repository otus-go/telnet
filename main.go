package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"time"
)

const (
	minArgs     = 1
	defaultPort = 80
	newLine     = "\r\n" // use CRLF for application level protocols
)

type Params struct {
	Host        string
	Port        int
	Timeout     time.Duration
	DialTimeout time.Duration
}

func (p Params) Address() string {
	return fmt.Sprintf("%s:%d", p.Host, p.Port)
}

func registerArgs(params *Params) {
	flag.DurationVar(&params.DialTimeout, "dial-timeout", 5*time.Second, "establishing connection time limit")
	flag.DurationVar(&params.Timeout, "timeout", 0, "interaction time limit")
}

func handlePositionalArgs(params *Params) error {
	if flag.NArg() < minArgs {
		return fmt.Errorf("please, pass at least %d positional arguments", minArgs)
	}
	params.Host = flag.Arg(0)
	params.Port = defaultPort
	var err error
	if flag.NArg() == 2 {
		if params.Port, err = strconv.Atoi(flag.Arg(1)); err != nil {
			return errors.New("incorrect port value")
		}
	}
	return nil
}

func init() {
	// disable standard log formatting
	log.SetFlags(0)
	// change default help message
	flag.Usage = func() {
		appName := os.Args[0]
		_, _ = fmt.Fprintf(os.Stdout, "Usage of %s:\n", appName)
		_, _ = fmt.Fprintf(os.Stdout, "\t%s [-timeout] [-dial-timeout] host [port] \n", appName)
		flag.PrintDefaults()
	}
}

func main() {

	// handle command line args and init params
	params := Params{}
	registerArgs(&params)
	flag.Parse()

	if err := handlePositionalArgs(&params); err != nil {
		log.Println(err)
		flag.Usage()
		os.Exit(1)
	}

	// establishing connection
	ipAddr, err := net.ResolveTCPAddr("tcp", params.Address())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Trying:", ipAddr)
	conn, err := net.DialTimeout("tcp", ipAddr.String(), params.DialTimeout)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	fmt.Println("Connected to:", params.Address())
	fmt.Println("Use ^C to exit")

	// creating context
	var (
		ctx    context.Context
		cancel context.CancelFunc
	)
	if params.Timeout > 0 {
		ctx, cancel = context.WithTimeout(context.Background(), params.Timeout)
	} else {
		ctx, cancel = context.WithCancel(context.Background())
	}

	// registering the channel for Keyboard Interrupt signal
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// run signal handler
	go func() {
		s := <-c
		log.Println("\nGot signal:", s)
		cancel()
	}()

	// run stdin reader
	out := make(chan string)
	go func(chan<- string) {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			out <- scanner.Text()
		}
		cancel()
	}(out)

	// run socket response reader
	in := make(chan string)
	go func(<-chan string) {
		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			in <- scanner.Text()
		}
		log.Println("Connection closed")
		cancel()
	}(in)

	// main loop
LOOP:
	for {
		select {
		case message := <-in:
			fmt.Println(message)
		case message := <-out:
			_, err := conn.Write([]byte(fmt.Sprintf("%s%s", message, newLine)))
			if err != nil {
				log.Println("Unable to send message")
				break LOOP
			}
		case <-ctx.Done():
			fmt.Println("Good bye!")
			break LOOP
		}
	}
}
